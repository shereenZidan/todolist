<?php

    require("../includes/config.php");

    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        $listId = $_GET["listId"];
        render("createTodo.php", ["title" => "create todo","listId" => $listId ]);
    }

    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["name"]))
        {
            apologize("You must enter a name.");
        }

        else
        {
            $name=$_POST["name"];
            $id=$_POST["listId"];
            $user_id=$_POST["iduser"];
            $result= query("INSERT INTO todo (name,list_id,user_id) VALUES ('$name','$id','$user_id')");
            if($result)
            {
                redirect("/todo.php?listId=".$id);
            }
            else
            {
                echo"query is not right!";
            }
        }
    }

?>
