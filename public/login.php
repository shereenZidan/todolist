<?php

    require("../includes/config.php");

    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("login.php", ["title" => "Log In"]);
    }

    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide your password.");
        }
        else {
          $username = $_POST['username'];
          
          $rows = query("SELECT * FROM user WHERE username = '$username'");

          if (count($rows) == 1)
          {
              $row = $rows[0];

              if (password_verify($_POST["password"], $row["hash"]))
              {
                  $_SESSION["id"] = $row["id"];

                  redirect("/");
              }
          }

          
          apologize("Invalid username and/or password.");
        }
    }

?>
