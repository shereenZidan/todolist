<table class="infotable">
    <h3>You are in the <?= $cat["name"] ?> category </h3>
    <?php 
    if($list)
    {
        
    
    foreach ($list as $lists): ?>
    <tr>
        <td> <?= $lists["name"] ?></td>
        <td> <a href="/todo.php?listId=<?= $lists['id'] ?>"> View Todo</a></td>
        <td> <a href="/createTodo.php?listId=<?= $lists['id'] ?>"> Create New Todo</a></td>
        <td> <a href="/updateList.php?listId=<?= $lists['id'] ?>&categoryId=<?= $categoryId ?>"> Update List</a></td>
        <td> <a href="/deleteList.php?listId=<?= $lists['id'] ?>&categoryId=<?= $categoryId ?>"> Delete List</a></td>
        
    </tr>
    
    <?php endforeach; }
    else
    {
        echo "<h3>You didn't created lists yet! Please click <a href=\"/createList.php?categoryId=".$categoryId."\">here</a> to create lists</h3>";
    }
    
    ?>

</table>