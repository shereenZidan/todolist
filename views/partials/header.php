<!DOCTYPE html>
<html>
    <head>

        <?php if (isset($title)): ?>
            <title>To-Do List: <?= $title ?></title>
        <?php else: ?>
            <title>ToDoList</title>
        <?php endif ?>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/css/styles.css" rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/scripts.js"></script>

    </head>

    <body>
        <?php if (!empty($_SESSION["id"])): ?>
         <nav id="nav">
         <section >
         <ul id="menu">
             
            <li class="btn btn-default"><a href="/logout.php"><i>Log Out</i></a></li>
            <li class="btn btn-default"><a href="reset.php"><i>Reset password</i></a></li>
                
         </ul>
        </section>

    </nav>
        <div class="container">

            <div id="top">
              
                <div>

                   <img alt="todo list img" src="/img/shereen.png"/>
                </div>




                    <ul class="nav nav-pills">
                        <li><a href="/">Categories</a></li>
                        <li><a href="/createCategory.php">Create Category</a></li>
                        
                    </ul>
                

            </div>
            
           
            <?php endif ?>

            <div id="middle">

        </body>