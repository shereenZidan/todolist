<table class="infotable">
<?php foreach ($categories as $category): ?>
    <tr>
        <td>
             <?= $category["name"] ?>
        </td>
        
        <td>
            <a href="/list.php?categoryId=<?= $category['id'] ?>">View Lists</a>
        </td>
        <td>
            <a href="/createList.php?categoryId=<?= $category['id'] ?>">Create New List</a>
            
        </td>
        <td>
             <a href="/updateCategory.php?categoryId=<?= $category['id'] ?>">Update Category</a>
        </td>
        <td>
            <a href="deleteCategory.php?categoryId=<?= $category['id'] ?>"> Delete Category </a>
        </td>
    </tr>

<?php endforeach ?>
</table>
