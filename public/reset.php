<?php

require("../includes/config.php");
if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("reset.php", ["title" => "reset"]);
    }

    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["password"]))
        {
            apologize("must enter the old password");
        }
        else if (empty($_POST["newpassword"]))
        {
            apologize("must enter a new password.");
        }
        else if (empty($_POST["confirmation"]))
        {
            apologize("must confirm your password.");
        }
        else if ($_POST['newpassword'] != $_POST['confirmation']) {
            apologize("Your password doesn't not match, please try again!");
        }
        else
        {
            $user_id= $_SESSION["id"];
            $password= $_POST["password"];
            $rows = query("SELECT * FROM user WHERE id = '$user_id'");
            if (count($rows) == 1)
          {
              $row = $rows[0];

              if (password_verify($_POST["password"], $row["hash"]))
              {
                  $hash = password_hash($_POST["newpassword"], PASSWORD_DEFAULT);
                  $result = query("UPDATE user SET hash= '$hash' ");
                  echo "Password changed.";
                  redirect("/");
              }
          }
            else
            {
                apologize("Your entered a wrong password");
            }
        }
    }

?>