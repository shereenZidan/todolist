<form action="login.php" method="post">
    
        <div class="form-group">
            <input class="form-control" autocomplete="on" name="username" placeholder="Username" type="text"/>
        </div>
        <div class="form-group">
            <input class="form-control" name="password" placeholder="Password" type="password"/>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
                Log In
            </button>
        </div>
    
</form>
<div>
    or <a href="register.php">register</a> if you don't have an account.
</div>
