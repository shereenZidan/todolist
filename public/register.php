<?php

    require("../includes/config.php");

    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("register.php", ["title" => "register"]);
    }

    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("must enter a username");
        }
        else if (empty($_POST["password"]))
        {
            apologize("must enter a password.");
        }
        else if (empty($_POST["confirmation"]))
        {
            apologize("must confirm your password.");
        }
        else if ($_POST['password'] != $_POST['confirmation']) {
            apologize("Your password doesn't not match, please try again!");
        }
        else
        {
            $username = $_POST["username"];
            $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $result = query("INSERT INTO user (username, hash) VALUES('$username', '$hash')");
            $row = query("SELECT id FROM user ORDER BY id DESC LIMIT 1;");
            $id = $row[0]['id'];
            $_SESSION["id"] = $id;
            redirect("/");
        }
    }

?>
